import React, { useState, useEffect} from "react";

import "./App.css";
import { Input, Row,  } from "reactstrap";

import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";

// import datos from "./datos.json";

import { FixedSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";

import Cabecera from "./components/Cabecera";
import Cards from "./components/Cards";
import FrasesConectivas from "./components/FrasesConectivas";
import Frases from "./components/Frases";
import ComponentMantenimiento from "./components/ComponentMantenimiento";
import Sufijos from "./components/Sufijos";
function App() {
  const [Palabra, setPalabra] = useState([]);
  const [tablaPalabra, setTablaPalabra] = useState([]);
  const [busqueda, setBusqueda] = useState("");

  function fetchData() {
    const url = 'https://www.wayuback.grupoda2.com/api/traductor/';

    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            // Asumiendo que setPalabra() y setTablaPalabra() son funciones que procesan los datos
            setPalabra(data);
            setTablaPalabra(data);
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
        });
}


  useEffect(() => {
    fetchData()
  }, []);



  

  const Row1 = ({ index, style }) => {
    const palabra = Palabra[index];
    return (
      <Popup
        className="pop-up pruebas"
        trigger={
          <button style={ style } key={palabra.id} className="btn-medidas">
  {languaje ? palabra.definicion : palabra.palabra}
</button>
        }
        modal
        nested
      >
        {(close) => (
          <div className="modal">
            <Row style={{ display: "flex", justifyContent: "flex-end" }}>
              <button   className="close btn-close" onClick={close}>
                &times;
              </button>
            </Row>
            <div className="header">
              Definicion : {languaje ? palabra.palabra : palabra.definicion}
            </div>
          </div>
        )}
      </Popup>
    );
  };

  const handleChange = (e) => {
    const terminoBusqueda = e.target.value;
    setBusqueda(terminoBusqueda);
    filtrar(terminoBusqueda);
  };

  const filtrar = (terminoBusqueda) => {
    const resultadoBusqueda = tablaPalabra.filter((elemento) => {
      if (languaje) {
        return elemento.definicion
          .toLowerCase()
          .includes(terminoBusqueda.toLowerCase());
      } else {
        return elemento.palabra
          .toLowerCase()
          .includes(terminoBusqueda.toLowerCase());
      }
    });

    setPalabra(resultadoBusqueda);
  };

  const [languaje, setlanguaje] = useState(false);
  const handleChangeLanguaje = () => {
    setlanguaje(!languaje);
  };


  const [show, setshow] = useState('0')

  const handleshow = (show) => {
    setshow(show)
  }



  return (
    <>
    <ComponentMantenimiento
    busqueda={busqueda}
    >

    </ComponentMantenimiento>

      <Cabecera
      handleshow={handleshow}
      ></Cabecera>

{show==='0'?(
     <Cards
     handleshow={handleshow}
     >
      
     </Cards>
):null}

    {show==='1'?(
      <React.Fragment>
      <div className="col-md-7" style={{marginTop:'60px'}}>
        <Input
          type="text"
          className="input-principal"
          placeholder="Escribe aqui la Palabra"
          onChange={handleChange}
          style={{ textTransform: "lowercase" }}
        />

        <button 
        className="btn-principal"
        onClick={handleChangeLanguaje}>
          {languaje ? "Wayu" : "Español"}
        </button>
      </div>

      <AutoSizer>
        {({ width, height }) => (
          <List
            height={height}
            itemCount={Palabra.length}
            itemSize={35}
            width={width}
          >
            {Row1}
          </List>
        )}
      </AutoSizer>
      </React.Fragment>
    ):null}


    {show==='3'?(
     <FrasesConectivas>

     </FrasesConectivas>
    ):null}

    {show==='4'?(
     <Frases>

     </Frases>
    ):null}

    {show==='5'?(
     <Sufijos>

     </Sufijos>
    ):null}

    </>
  );
}

export default App;
