
import React, {useState} from 'react'

import  './Cabecera.css'


const Cabecera = (props) => {

    const [showSpan, setShowSpan] = useState(false);

    const toggleSpan = () => {
      setShowSpan(!showSpan);
    };
    return ( 

        <React.Fragment>


        <nav className='mobile-nav'>
              
        <li
              onClick={()=>{ props.handleshow('0')}}
       
            style={{color:'white', listStyle:'none', position:'absolute', left:'20px', top:'16px'}}
            >W</li>

            <button className='mobileButton' onClick={toggleSpan}>
            <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className=" svg-button" viewBox="0 0 16 16">
        <path fillRule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
        </svg>
                </span>   

                {showSpan && <span className='navBarButton-text srText'></span>}
        </button>
        {showSpan && 
        <div className="mobileNav-menu" >
            

            <li
             className='li-mobile-menu'

             onClick={()=>{ 
              props.handleshow('1')
              setShowSpan(!showSpan);
            }}
        
            >DICCIONARIO</li>
           
            <li
               
              onClick={()=>{ 
                props.handleshow('3')
                setShowSpan(!showSpan);
              }}
             className='li-mobile-menu'
           
            >FRASES CONECTIVAS</li>
            <li
               
              onClick={()=>{ 
                props.handleshow('4')
                setShowSpan(!showSpan);
              }}
             className='li-mobile-menu'
           
            >FRASES </li>
            <li
               
              onClick={()=>{ 
                props.handleshow('5')
                setShowSpan(!showSpan);
              }}
             className='li-mobile-menu'
           
            >SUFIJOS </li>
           
        </div>

        }
        </nav>






        <nav 
        className='nav-desktop'
        style={{
            backgroundColor:'#292929',
            height:'50px',
            
        
        }}>
           
            <ul
            style={{display:'flex', justifyContent:'space-evenly'}}
            >
              
              <li
              onClick={()=>{ props.handleshow('0')}}
            className='li'
            style={{color:'white', listStyle:'none', position:'absolute', left:'20px'}}
            >W</li>

            <li
              onClick={()=>{ props.handleshow('1')}}
            className='li'
            style={{color:'white', listStyle:'none'}}
            >DICCIONARIO</li>
           
            <li
              
              onClick={()=>{ props.handleshow('3')}}
                  className='li'
              style={{color:'white', listStyle:'none'}}
            >FRASES CONECTIVAS</li>
            </ul>
        </nav>
        </React.Fragment>
     );
}
 
export default Cabecera;