import React from "react";
import { Col, Row } from "reactstrap";

import "./Cards.css";
const Cards = (props) => {
  return (
    <React.Fragment>
      <Row className="row-cards">
        <Col
          onClick={() => {
            props.handleshow("1");
          }}
          className="col-card"
        >
          DICCIONARIO
        </Col>
    
        <Col
          onClick={() => {
            props.handleshow("3");
          }}
          className="col-card"
        >
          FRASES CONECTIVAS
        </Col>
      </Row>

      <Row className="row-cards">
        <Col
          onClick={() => {
            props.handleshow("4");
          }}
          className="col-card"
        >
          FRASES
        </Col>

        <Col
          onClick={() => {
            props.handleshow("5");
          }}
          className="col-card"
        >
          SUFIJOS
        </Col>
     
      </Row>
  
    </React.Fragment>
  );
};

export default Cards;
