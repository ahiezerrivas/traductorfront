import React, {useState, useEffect} from 'react'
import { Input,  Row } from "reactstrap";

import Popup from "reactjs-popup";

import { FixedSizeList as List } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import ComponentMantenimiento from './ComponentMantenimiento';
function Frases() {

    const [Palabra, setPalabra] = useState([]);
    const [busqueda, setBusqueda] = useState("");
    const [tablaPalabra, setTablaPalabra] = useState([]);
    const [languaje, setlanguaje] = useState(false);


    function fetchData() {
      const url = 'https://www.wayuback.grupoda2.com/api/frases/';
  
      fetch(url)
          .then(response => {
              if (!response.ok) {
                  throw new Error('Network response was not ok');
              }
              return response.json();
          })
          .then(data => {
              // Asumiendo que setPalabra() y setTablaPalabra() son funciones que procesan los datos
              setPalabra(data);
              setTablaPalabra(data);
          })
          .catch(error => {
              console.error('There was a problem with the fetch operation:', error);
          });
  }

    useEffect(() => {
      fetchData()
      }, []);

    const handleChange = (e) => {
        const terminoBusqueda = e.target.value;
        setBusqueda(terminoBusqueda);
        filtrar(terminoBusqueda);
      };

      const filtrar = (terminoBusqueda) => {
        const resultadoBusqueda = tablaPalabra.filter((elemento) => {
          if (languaje) {
            return elemento.definicion
              .toLowerCase()
              .includes(terminoBusqueda.toLowerCase());
          } else {
            return elemento.frase
              .toLowerCase()
              .includes(terminoBusqueda.toLowerCase());
          }
        });
    
        setPalabra(resultadoBusqueda);
      };
      

      const handleChangeLanguaje = () => {
        setlanguaje(!languaje);
      };
    
    

      const Row1 = ({ index, style }) => {
        const palabra = Palabra[index];
        return (
          <Popup
            className="pop-up pruebas"
            trigger={
              <button style={ style } key={palabra.id} className="btn-medidas">
      {languaje ? palabra.definicion : palabra.frase}
    </button>
            }
            modal
            nested
          >
            {(close) => (
              <div className="modal">
                <Row style={{ display: "flex", justifyContent: "flex-end" }}>
                  <button   className="close btn-close" onClick={close}>
                    &times;
                  </button>
                </Row>
                <div className="header">
                  Definicion : {languaje ? palabra.frase : palabra.definicion}
                </div>
              </div>
            )}
          </Popup>
        );
      };


  return (
   <React.Fragment>

    <ComponentMantenimiento
    busqueda={busqueda}
    >

    </ComponentMantenimiento>
    
    <div className="col-md-7" style={{marginTop:'60px'}}>
        <Input
          type="text"
          className="input-principal "
          placeholder="Escribe aqui la Frase"
          onChange={handleChange}
          style={{ textTransform: "lowercase" }}
        />

        <button 
        className='btn-principal'
        onClick={handleChangeLanguaje}>
          {languaje ? "Wayu" : "Español"}
        </button>
      
    </div>
    <AutoSizer>
        {({ width, height }) => (
          <List
            height={height}
            itemCount={Palabra.length}
            itemSize={35}
            width={width}
          >
            {Row1}
          </List>
        )}
      </AutoSizer>
    </React.Fragment>
  )
}

export default Frases
